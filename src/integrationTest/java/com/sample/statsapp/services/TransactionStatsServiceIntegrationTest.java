package com.sample.statsapp.services;

import com.sample.statsapp.models.IndexedStatsWindowHolder;
import com.sample.statsapp.models.TransactionRecord;
import com.sample.statsapp.models.TransactionStats;
import com.sample.statsapp.models.exceptions.StaleTransactionException;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class TransactionStatsServiceIntegrationTest {


    private DateTime timeAtStart;
    private TransactionStatsService transactionStatsService;

    @Before
    public void setUp() {
        timeAtStart = DateTime.now();
    }

    @Test
    public void shouldReturnStatsOfLast60Seconds() throws Exception {
        transactionStatsService = new TransactionStatsService(new IndexedStatsWindowHolder(1000, 20000));
        TransactionStats statsForActiveDuration = transactionStatsService.getStatsForActiveDurationFrom(DateTime.now());
        assertStatsFor(statsForActiveDuration, 0, 0, 0, 0);

        saveTransaction(120D, timeFromStartInMillis(0));

        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(0)), 120D, 120D, 120D, 1);

        saveTransaction(250D, timeFromStartInMillis(1000));
        saveTransaction(500D, timeFromStartInMillis(2000));
        saveTransaction(280D, timeFromStartInMillis(5000));

        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(12000)), 1150D, 500D, 120D, 4);

        saveTransaction(900D, timeFromStartInMillis(14100));
        saveTransaction(160D, timeFromStartInMillis(18000));

        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(18000)), 2210D, 900D, 120D, 6);

        saveTransaction(400D, timeFromStartInMillis(21000));

        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(21000)), 2490D, 900D, 160D, 6);
        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(25000)), 1740D, 900D, 160D, 4);
        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(35000)), 560D, 400D, 160D, 2);
    }

    @Test
    public void shouldReturnStatsOfLast60SecondsWithMillisecondAccuracy() throws Exception {
        transactionStatsService = new TransactionStatsService(new IndexedStatsWindowHolder(1, 20000));
        TransactionStats statsForActiveDuration = transactionStatsService.getStatsForActiveDurationFrom(DateTime.now());
        assertStatsFor(statsForActiveDuration, 0, 0, 0, 0);

        saveTransaction(120D, timeFromStartInMillis(0));

        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(DateTime.now()), 120D, 120D, 120D, 1);

        saveTransaction(250.5D, timeFromStartInMillis(5500));
        saveTransaction(500D, timeFromStartInMillis(5501));
        saveTransaction(280D, timeFromStartInMillis(5502));

        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(12000)), 1150.5D, 500D, 120D, 4);
        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(20000)), 1150.5D, 500D, 120D, 4);
        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(20001)), 1030.5D, 500D, 250.5D, 3);
        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(25500)), 1030.5D, 500D, 250.5D, 3);
        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(25501)), 780D, 500D, 280D, 2);
        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(25502)), 280D, 280D, 280D, 1);
        assertStatsFor(transactionStatsService.getStatsForActiveDurationFrom(timeFromStartInMillis(25503)), 0, 0, 0, 0);
    }

    private void saveTransaction(double transactionAmount, DateTime timeFromStartInMillis) throws StaleTransactionException {
        this.transactionStatsService.saveTransaction(new TransactionRecord(transactionAmount, timeFromStartInMillis));
    }

    private DateTime timeFromStartInMillis(int i) {
        return timeAtStart.plusMillis(i);
    }

    private void assertStatsFor(TransactionStats statsForActiveDuration, double totalAmount, double maximumAmount, double minimumAmount, int transactionCount) {
        assertThat(statsForActiveDuration.getTransactionCount(), is(transactionCount));
        assertThat(statsForActiveDuration.getTotalAmount(), is(totalAmount));
        assertThat(statsForActiveDuration.getMinimumAmount(), is(minimumAmount));
        assertThat(statsForActiveDuration.getMaximumAmount(), is(maximumAmount));
    }
}
