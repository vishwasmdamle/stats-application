package com.sample.statsapp.services;

import com.google.common.collect.ImmutableMap;
import com.sample.statsapp.StatsApiApplication;
import com.sample.statsapp.StatsApiConfiguration;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.eclipse.jetty.http.HttpStatus;
import org.glassfish.jersey.client.ClientResponse;
import org.hamcrest.core.Is;
import org.joda.time.DateTime;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class StatsResourceIntegrationTest {

    @ClassRule
    public static final DropwizardAppRule<StatsApiConfiguration> RULE =
            new DropwizardAppRule<>(StatsApiApplication.class);

    @Test
    public void shouldCaptureTransactionsAndReturnStats() {
        Client client = RULE.client();
        WebTarget transactionTarget = client.target("http://localhost:" + RULE.getLocalPort() + "/transactions");
        Response response = transactionTarget.request()
                .post(Entity.entity(of("amount", 234.5, "timestamp", DateTime.now()), MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus(), is(HttpStatus.CREATED_201));
        response = transactionTarget.request()
                .post(Entity.entity(of("amount", 266.5, "timestamp", DateTime.now().minusMillis(2134)), MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus(), is(HttpStatus.CREATED_201));
        response = transactionTarget.request()
                .post(Entity.entity(of("amount", 400, "timestamp", DateTime.now().minusMillis(4000)), MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus(), is(HttpStatus.CREATED_201));
        response = transactionTarget.request()
                .post(Entity.entity(of("amount", 20, "timestamp", DateTime.now().minusMillis(40)), MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus(), is(HttpStatus.CREATED_201));

        response = client.target("http://localhost:" + RULE.getLocalPort() + "/stats").request()
                .get();

        assertThat(response.getStatus(), is(HttpStatus.OK_200));
        assertThat(response.readEntity(Map.class), is(of("sum", 921.0, "max", 400.0, "min", 20.0, "avg", 230.25, "count", 4)));
    }
}
