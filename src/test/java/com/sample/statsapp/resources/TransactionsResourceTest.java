package com.sample.statsapp.resources;

import com.google.common.collect.ImmutableMap;
import com.sample.statsapp.models.exceptions.StaleTransactionException;
import com.sample.statsapp.models.TransactionRecord;
import com.sample.statsapp.services.TransactionStatsService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static com.google.common.collect.ImmutableMap.of;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.Status.*;
import static org.eclipse.jetty.http.HttpStatus.Code.UNPROCESSABLE_ENTITY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.joda.time.DateTime.now;
import static org.mockito.Mockito.*;

public class TransactionsResourceTest {

    private static final TransactionStatsService mockTransactionStatsService = mock(TransactionStatsService.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new TransactionsResource(mockTransactionStatsService))
            .build();

    @Before
    public void setUp() {
        Mockito.reset(mockTransactionStatsService);
    }

    @Test
    public void shouldReturn201IfTransactionIsSavedSuccessfully() throws Exception {
        long timestamp = now().getMillis();
        ImmutableMap<String, Object> requestMap = of("amount", 123.4D, "timestamp", timestamp);

        Response response = resources.client().target("/transactions").request().post(Entity.entity(requestMap, APPLICATION_JSON_TYPE));

        ArgumentCaptor<TransactionRecord> argumentCaptor = ArgumentCaptor.forClass(TransactionRecord.class);
        verify(mockTransactionStatsService).saveTransaction(argumentCaptor.capture());
        TransactionRecord transactionRecord = argumentCaptor.getValue();
        assertThat(transactionRecord.amount, is(123.4D));
        assertThat(transactionRecord.timestamp.getMillis(), is(timestamp));
        assertThat(response.getStatus(), is(CREATED.getStatusCode()));
    }

    @Test
    public void shouldReturn204IfStaleTransactionExceptionIsRaised() throws Exception {
        long timestamp = now().getMillis();
        ImmutableMap<String, Object> requestMap = of("amount", 123.4D, "timestamp", timestamp);
        doThrow(new StaleTransactionException()).when(mockTransactionStatsService).saveTransaction(any());

        Response response = resources.client().target("/transactions").request().post(Entity.entity(requestMap, APPLICATION_JSON_TYPE));

        assertThat(response.getStatus(), is(NO_CONTENT.getStatusCode()));
    }

    @Test
    public void shouldReturn400IfRequestBodyIsNotValid() {
        ImmutableMap<String, Object> requestMap = of("amount", 123.4D);

        Response response = resources.client().target("/transactions").request().post(Entity.entity(requestMap, APPLICATION_JSON_TYPE));

        assertThat(response.getStatus(), is(UNPROCESSABLE_ENTITY.getCode()));
    }

    @Test
    public void shouldReturn422IfRequestBodyIsEmpty() {
        Response response = resources.client().target("/transactions").request().post(null);

        assertThat(response.getStatus(), is(UNPROCESSABLE_ENTITY.getCode()));
    }
}