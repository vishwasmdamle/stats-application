package com.sample.statsapp.resources;

import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.eclipse.jetty.http.HttpStatus.OK_200;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


public class PingResourceTest {

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new PingResource())
            .build();

    @Test
    public void shouldReturnPong() { //Test to get test setup working
        Response response = resources.client().target("/ping").request().get();

        assertThat(response.getStatus(), is(OK_200));
        assertThat(response.readEntity(String.class), is("pong"));
    }
}