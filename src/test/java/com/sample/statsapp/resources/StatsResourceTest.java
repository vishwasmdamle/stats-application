package com.sample.statsapp.resources;

import com.sample.statsapp.models.TransactionStats;
import com.sample.statsapp.models.boundary.TransactionStatsResponse;
import com.sample.statsapp.services.TransactionStatsService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.eclipse.jetty.http.HttpStatus;
import org.joda.time.DateTime;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;

public class StatsResourceTest {


    private static final TransactionStatsService mockTransactionStatsService = mock(TransactionStatsService.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new StatsResource(mockTransactionStatsService))
            .build();

    @Test
    public void shouldReturnStatsForLastMinute() {
        when(mockTransactionStatsService.getStatsForActiveDurationFrom(any())).thenReturn(TransactionStats.with(20000, 300, 10, 50));

        Response response = resources.client().target("/stats").request().get();

        verify(mockTransactionStatsService).getStatsForActiveDurationFrom(any()); //Check if this can be asserted better
        assertThat(response.getStatus(), is(HttpStatus.OK_200));
        TransactionStatsResponse receivedStats = response.readEntity(TransactionStatsResponse.class);
        assertThat(receivedStats.sum, is(20000D));
        assertThat(receivedStats.max, is(300D));
        assertThat(receivedStats.min, is(10D));
        assertThat(receivedStats.avg, is(400D));
        assertThat(receivedStats.count, is(50));
    }

}