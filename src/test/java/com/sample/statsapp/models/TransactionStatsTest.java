package com.sample.statsapp.models;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class TransactionStatsTest {
    @Test
    public void shouldUpdateSumMaxMinAndCountWhenATransactionIsPushed() {
        TransactionStats transactionStats = TransactionStats.zeroStats();
        transactionStats.updateStats(new TransactionRecord(250D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(290D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(10D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(100D, DateTime.now()));

        assertThat(transactionStats.getTotalAmount(), is(650D));
        assertThat(transactionStats.getMaximumAmount(), is(290D));
        assertThat(transactionStats.getMinimumAmount(), is(10D));
        assertThat(transactionStats.getTransactionCount(), is(4));

        transactionStats.updateStats(new TransactionRecord(60D, DateTime.now()));

        assertThat(transactionStats.getTotalAmount(), is(710D));
        assertThat(transactionStats.getMaximumAmount(), is(290D));
        assertThat(transactionStats.getMinimumAmount(), is(10D));
        assertThat(transactionStats.getTransactionCount(), is(5));
    }

    @Test
    public void shouldMergeAnotherTransactionStatsObject() {
        TransactionStats transactionStats = TransactionStats.zeroStats();
        TransactionStats transactionStatsToMerge = TransactionStats.zeroStats();
        transactionStats.updateStats(new TransactionRecord(250D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(290D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(10D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(100D, DateTime.now()));
        transactionStatsToMerge.updateStats(new TransactionRecord(150D, DateTime.now()));
        transactionStatsToMerge.updateStats(new TransactionRecord(140D, DateTime.now()));

        transactionStats.merge(transactionStatsToMerge);

        assertThat(transactionStats.getTotalAmount(), is(940D));
        assertThat(transactionStats.getMaximumAmount(), is(290D));
        assertThat(transactionStats.getMinimumAmount(), is(10D));
        assertThat(transactionStats.getTransactionCount(), is(6));
    }

    @Test
    public void shouldMergeWhenAnotherTransactionStatsWithZeroTransactions() {
        TransactionStats transactionStats = TransactionStats.zeroStats();
        TransactionStats transactionStatsToMerge = TransactionStats.zeroStats();
        transactionStats.updateStats(new TransactionRecord(250D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(290D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(10D, DateTime.now()));
        transactionStats.updateStats(new TransactionRecord(100D, DateTime.now()));

        transactionStats.merge(transactionStatsToMerge);

        assertThat(transactionStats.getTotalAmount(), is(650D));
        assertThat(transactionStats.getMaximumAmount(), is(290D));
        assertThat(transactionStats.getMinimumAmount(), is(10D));
        assertThat(transactionStats.getTransactionCount(), is(4));
    }
    @Test
    public void shouldMergeAnotherTransactionStatsWhenCurrentTransactionStatsHasZeroTransactions() {
        TransactionStats transactionStats = TransactionStats.zeroStats();
        TransactionStats transactionStatsToMerge = TransactionStats.zeroStats();
        transactionStatsToMerge.updateStats(new TransactionRecord(250D, DateTime.now()));
        transactionStatsToMerge.updateStats(new TransactionRecord(290D, DateTime.now()));
        transactionStatsToMerge.updateStats(new TransactionRecord(10D, DateTime.now()));
        transactionStatsToMerge.updateStats(new TransactionRecord(100D, DateTime.now()));

        transactionStats.merge(transactionStatsToMerge);

        assertThat(transactionStats.getTotalAmount(), is(650D));
        assertThat(transactionStats.getMaximumAmount(), is(290D));
        assertThat(transactionStats.getMinimumAmount(), is(10D));
        assertThat(transactionStats.getTransactionCount(), is(4));
    }
}