package com.sample.statsapp.models;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class IndexedStatsWindowHolderTest {

    private final IndexedStatsWindowHolder windowHolder = new IndexedStatsWindowHolder(1000, 60000);
    private DateTime timeAtStart;

    @Before
    public void setUp() throws Exception {
        timeAtStart = DateTime.now();
    }

    @Test
    public void shouldCreateAndReturnWindowForGivenTimestampIfNotExists() {
        DateTime currentTimestamp = DateTime.now();

        StatsWindow window = windowHolder.getWindowFor(currentTimestamp);

        assertTrue(window.isTimestampWithinWindow(currentTimestamp));
    }

    @Test
    public void shouldCreateAndReturnNewWindowForGivenTimestampIfTimestampDoesNotFallInExistingWindows() {
        StatsWindow someOldWindow = windowHolder.getWindowFor(timeAtStart.minusSeconds(5));
        DateTime currentTimestamp = timeAtStart;

        StatsWindow window = windowHolder.getWindowFor(currentTimestamp);

        assertTrue(window.isTimestampWithinWindow(currentTimestamp));
        assertNotEquals(window, someOldWindow);
    }

    @Test
    public void shouldReturnSameWindowForGivenTimestampIfAWindowInWhichThisTimestampFallsExists() {
        StatsWindow firstWindow = windowHolder.getWindowFor(timeAtStart);

        assertThat(windowHolder.getWindowFor(timeAtStart.minusMillis(20)), is(firstWindow));
    }

    @Test
    public void shouldReturnAllWindowsFromGivenTimestampForMaxDuration() {
        StatsWindow oldWindow1 = windowHolder.getWindowFor(timeAtStart.minusSeconds(65));
        StatsWindow oldWindow2 = windowHolder.getWindowFor(timeAtStart.minusSeconds(61));
        StatsWindow activeWindow1 = windowHolder.getWindowFor(timeAtStart.minusSeconds(40));
        StatsWindow activeWindow2 = windowHolder.getWindowFor(timeAtStart.minusSeconds(20));
        StatsWindow activeWindow3 = windowHolder.getWindowFor(timeAtStart.minusSeconds(20));
        StatsWindow activeWindow4 = windowHolder.getWindowFor(timeAtStart.minusSeconds(1));
        StatsWindow activeWindow5 = windowHolder.getWindowFor(timeAtStart);

        List<StatsWindow> activeWindows = windowHolder.getActiveWindowsBefore(timeAtStart);

        assertThat(activeWindows, hasItem(activeWindow5));
        assertThat(activeWindows, hasItem(activeWindow4));
        assertThat(activeWindows, hasItem(activeWindow3));
        assertThat(activeWindows, hasItem(activeWindow2));
        assertThat(activeWindows, hasItem(activeWindow1));
        assertThat(activeWindows, not(hasItem(oldWindow1)));
        assertThat(activeWindows, not(hasItem(oldWindow2)));
    }
}