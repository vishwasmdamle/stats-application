package com.sample.statsapp.services;

import com.sample.statsapp.models.IndexedStatsWindowHolder;
import com.sample.statsapp.models.StatsWindow;
import com.sample.statsapp.models.TransactionRecord;
import com.sample.statsapp.models.TransactionStats;
import com.sample.statsapp.models.exceptions.StaleTransactionException;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TransactionStatsServiceTest {

    private IndexedStatsWindowHolder mockStatsWindowHolder = mock(IndexedStatsWindowHolder.class);
    private final TransactionStatsService transactionStatsService = new TransactionStatsService(mockStatsWindowHolder);

    @Test(expected = StaleTransactionException.class)
    public void shouldRaiseAnExceptionIfTransactionIsOlderThan60Seconds() throws Exception {
        when(mockStatsWindowHolder.getWindowFor(any())).thenReturn(null);

        try {
            transactionStatsService.saveTransaction(new TransactionRecord(200D, DateTime.now().minusSeconds(65)));
        } catch (Exception e) {
            verify(mockStatsWindowHolder, never()).getWindowFor(any());
            throw e;
        }

    }

    @Test
    public void shouldSaveTransactionInAppropriateWindow() throws Exception {
        DateTime transactionTimestamp = DateTime.now().minusMillis(10145);
        StatsWindow applicableWindow = mock(StatsWindow.class);
        when(mockStatsWindowHolder.getWindowFor(transactionTimestamp)).thenReturn(applicableWindow);
        TransactionRecord newTransactionRecord = new TransactionRecord(200D, transactionTimestamp);

        transactionStatsService.saveTransaction(newTransactionRecord);

        verify(applicableWindow).pushTransactionInWindow(newTransactionRecord);
    }

    @Test
    public void shouldMergeWindowStatsIntoOneAndReturn() {
        StatsWindow mockWindow1 = mock(StatsWindow.class);
        StatsWindow mockWindow2 = mock(StatsWindow.class);
        StatsWindow mockWindow3 = mock(StatsWindow.class);
        when(mockWindow1.getTransactionStatsResponseForWindow()).thenReturn(TransactionStats.with(1000, 500, 40, 6));
        when(mockWindow2.getTransactionStatsResponseForWindow()).thenReturn(TransactionStats.with(1200, 600, 20, 8));
        when(mockWindow3.getTransactionStatsResponseForWindow()).thenReturn(TransactionStats.with(1000, 200, 100, 4));
        when(mockStatsWindowHolder.getActiveWindowsBefore(any())).thenReturn(Arrays.asList(mockWindow1, mockWindow2, mockWindow3));

        TransactionStats statsForOneMinuteSince = transactionStatsService.getStatsForActiveDurationFrom(DateTime.now());

        assertThat(statsForOneMinuteSince.getTotalAmount(), is(3200.0D));
        assertThat(statsForOneMinuteSince.getMaximumAmount(), is(600.0D));
        assertThat(statsForOneMinuteSince.getMinimumAmount(), is(20.0D));
        assertThat(statsForOneMinuteSince.getTransactionCount(), is(18));
    }
}