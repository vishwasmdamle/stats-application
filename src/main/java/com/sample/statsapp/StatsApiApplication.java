package com.sample.statsapp;

import com.sample.statsapp.resources.PingResource;
import com.sample.statsapp.resources.StatsResource;
import com.sample.statsapp.resources.TransactionsResource;
import com.sample.statsapp.services.TransactionStatsService;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class StatsApiApplication extends Application<StatsApiConfiguration> {
    public static void main(String[] args) throws Exception {
        new StatsApiApplication().run(args);
    }

    @Override
    public String getName() {
        return "stats-api";
    }

    @Override
    public void initialize(Bootstrap<StatsApiConfiguration> configuration) {
    }

    @Override
    public void run(StatsApiConfiguration configuration, Environment environment) {
        TransactionStatsService transactionStatsService = new TransactionStatsService();

        environment.jersey().register(new PingResource());
        environment.jersey().register(new TransactionsResource(transactionStatsService));
        environment.jersey().register(new StatsResource(transactionStatsService));
    }

}