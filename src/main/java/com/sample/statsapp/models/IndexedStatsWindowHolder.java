package com.sample.statsapp.models;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class IndexedStatsWindowHolder {
    private Map<Long, StatsWindow> statsWindowHash = new HashMap<>();
    private List<Long> windowStartTimestamps = new ArrayList<>();

    private final int windowDurationInMillis;
    private int maxDurationToMaintain;

    private static final int MARGIN_DURATION = 5000;

    public IndexedStatsWindowHolder(int windowDurationInMillis, int maxDurationToMaintain) {
        this.windowDurationInMillis = windowDurationInMillis;
        this.maxDurationToMaintain = maxDurationToMaintain;
    }

    public synchronized StatsWindow getWindowFor(DateTime timestamp) {
        StatsWindow statsWindow = statsWindowHash.get(getWindowStartTimestamp(timestamp));
        if (statsWindow == null) {
            statsWindow = new StatsWindow(getWindowStartTimestamp(timestamp), windowDurationInMillis);
            insertWindow(statsWindow);
            deleteStaleWindowsBefore(DateTime.now().minusMillis(maxDurationToMaintain + MARGIN_DURATION));
        }
        return statsWindow;
    }

    public synchronized List<StatsWindow> getActiveWindowsBefore(DateTime timestamp) {
        DateTime timeBeforeMaxDurationToMaintain = timestamp.minusMillis(maxDurationToMaintain + windowDurationInMillis);
        return windowStartTimestamps.stream()
                .filter(windowStartTimestamp -> new DateTime(windowStartTimestamp).isAfter(timeBeforeMaxDurationToMaintain))
                .map(windowStartTimestamp -> statsWindowHash.get(windowStartTimestamp))
                .collect(Collectors.toList());
    }

    private void insertWindow(StatsWindow window) {
        long windowStartTimestamp = window.getStartTimestamp();
        statsWindowHash.put(windowStartTimestamp, window);
        windowStartTimestamps.add(windowStartTimestamp);
    }

    private long getWindowStartTimestamp(DateTime timestamp) {
        return timestamp.getMillis() - timestamp.getMillis() % windowDurationInMillis;
    }

    private void deleteStaleWindowsBefore(DateTime timestamp) {
        List<Long> keysOfWindowsToDelete = windowStartTimestamps.stream()
                .filter(startTimestamp -> new DateTime(startTimestamp).isBefore(timestamp))
                .collect(Collectors.toList());
        keysOfWindowsToDelete.forEach(startTimestamp -> statsWindowHash.remove(startTimestamp));
        windowStartTimestamps.removeAll(keysOfWindowsToDelete);
    }
}
