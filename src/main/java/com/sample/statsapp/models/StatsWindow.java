package com.sample.statsapp.models;

import org.joda.time.DateTime;

public class StatsWindow {

    private long startTimestamp;
    private long endTimestamp;
    private TransactionStats transactionStatsResponseForWindow = TransactionStats.zeroStats();

    StatsWindow(long startTimestamp, int windowDurationInMillis) {
        this.startTimestamp = startTimestamp;
        this.endTimestamp = startTimestamp + windowDurationInMillis;
    }

    public TransactionStats getTransactionStatsResponseForWindow() {
        return transactionStatsResponseForWindow;
    }

    long getStartTimestamp() {
        return startTimestamp;
    }

    boolean isTimestampWithinWindow(DateTime timestamp) {
        return timestamp.getMillis() <= endTimestamp && timestamp.getMillis() > startTimestamp;
    }

    public void pushTransactionInWindow(TransactionRecord transactionRecord) {
        transactionStatsResponseForWindow.updateStats(transactionRecord);
    }
}
