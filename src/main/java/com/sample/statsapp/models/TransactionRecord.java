package com.sample.statsapp.models;

import org.joda.time.DateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

public class TransactionRecord {

    @NotNull
    public Double amount;

    @NotNull
    @Past
    public DateTime timestamp;

    @SuppressWarnings("unused")
    public TransactionRecord() {
        /* For deserialization */
    }

    public TransactionRecord(Double amount, DateTime timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }
}
