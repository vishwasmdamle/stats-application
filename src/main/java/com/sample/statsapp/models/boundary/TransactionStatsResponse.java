package com.sample.statsapp.models.boundary;

import com.sample.statsapp.models.TransactionStats;

public class TransactionStatsResponse {
    public double sum;

    public double max;

    public double min;

    public double avg;

    public int count;


    @SuppressWarnings("unused")
    public TransactionStatsResponse() {
        /* For deserialization */
    }

    public TransactionStatsResponse(double sum, double max, double min, double avg, int count) {
        this.sum = sum;
        this.max = max;
        this.min = min;
        this.avg = avg;
        this.count = count;
    }

    public static TransactionStatsResponse from(TransactionStats transactionStats) {
        return new TransactionStatsResponse(transactionStats.getTotalAmount(), transactionStats.getMaximumAmount(),
                transactionStats.getMinimumAmount(), getAverage(transactionStats),
                transactionStats.getTransactionCount());
    }

    private static double getAverage(TransactionStats transactionStats) {
        return transactionStats.getTransactionCount() > 0 ? transactionStats.getTotalAmount() / transactionStats.getTransactionCount() : 0;
    }
}
