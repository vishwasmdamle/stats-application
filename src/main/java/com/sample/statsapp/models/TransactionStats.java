package com.sample.statsapp.models;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class TransactionStats {
    private double totalAmount;

    private double maximumAmount;

    private double minimumAmount;

    private int transactionCount;


    @SuppressWarnings("unused")
    public TransactionStats() {
        /* For deserialization */
    }

    private TransactionStats(double totalAmount, double maximumAmount, double minimumAmount, int transactionCount) {
        this.totalAmount = totalAmount;
        this.maximumAmount = maximumAmount;
        this.minimumAmount = minimumAmount;
        this.transactionCount = transactionCount;
    }

    public static TransactionStats with(int totalAmount, int maximumAmount, int minimumAmount, int transactionCount) {
        return new TransactionStats(totalAmount, maximumAmount, minimumAmount, transactionCount);
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public double getMaximumAmount() {
        return maximumAmount;
    }

    public double getMinimumAmount() {
        return minimumAmount;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public static TransactionStats zeroStats() {
        return new TransactionStats(0, 0, 0, 0);
    }

    public void updateStats(TransactionRecord transactionRecord) {
        totalAmount += transactionRecord.amount;
        maximumAmount = max(transactionRecord.amount, maximumAmount);
        minimumAmount = transactionCount > 0 ? min(transactionRecord.amount, minimumAmount) : transactionRecord.amount;
        transactionCount++;
    }

    public TransactionStats merge(TransactionStats transactionStats) {
        if (transactionStats.getTransactionCount() == 0) return this;
        totalAmount += transactionStats.getTotalAmount();
        maximumAmount = max(transactionStats.getMaximumAmount(), maximumAmount);
        minimumAmount = transactionCount > 0 ? min(transactionStats.getMinimumAmount(), minimumAmount) : transactionStats.getMinimumAmount();
        transactionCount +=transactionStats.getTransactionCount();
        return this;
    }
}

