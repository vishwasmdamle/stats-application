package com.sample.statsapp.services;

import com.sample.statsapp.models.IndexedStatsWindowHolder;
import com.sample.statsapp.models.StatsWindow;
import com.sample.statsapp.models.TransactionRecord;
import com.sample.statsapp.models.TransactionStats;
import com.sample.statsapp.models.exceptions.StaleTransactionException;
import org.joda.time.DateTime;

import java.util.List;

public class TransactionStatsService {

    private static final int MAX_STATS_DURATION_IN_MILLIS = 60000;
    private static final int WINDOW_DURATION_IN_MILLIS = 1000;
    private IndexedStatsWindowHolder indexedStatsWindowHolder;

    public TransactionStatsService() {
        this(new IndexedStatsWindowHolder(WINDOW_DURATION_IN_MILLIS, MAX_STATS_DURATION_IN_MILLIS));
    }

    TransactionStatsService(IndexedStatsWindowHolder indexedStatsWindowHolder) {
        this.indexedStatsWindowHolder = indexedStatsWindowHolder;
    }

    public void saveTransaction(TransactionRecord transactionRecord) throws StaleTransactionException {
        validateTransactionTimestamp(transactionRecord, DateTime.now());
        indexedStatsWindowHolder.getWindowFor(transactionRecord.timestamp).pushTransactionInWindow(transactionRecord);
    }

    public TransactionStats getStatsForActiveDurationFrom(DateTime timestamp) {
        List<StatsWindow> activeWindows = indexedStatsWindowHolder.getActiveWindowsBefore(timestamp);
        return activeWindows.stream()
                .map(StatsWindow::getTransactionStatsResponseForWindow)
                .reduce(TransactionStats.zeroStats(), TransactionStats::merge);
    }

    private void validateTransactionTimestamp(TransactionRecord transactionRecord, DateTime currentTimestamp) throws StaleTransactionException {
        if (transactionRecord.timestamp.isBefore(currentTimestamp.minusMillis(MAX_STATS_DURATION_IN_MILLIS))) {
            throw new StaleTransactionException();
        }
    }

}
