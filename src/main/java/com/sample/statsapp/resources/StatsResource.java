package com.sample.statsapp.resources;

import com.codahale.metrics.annotation.Timed;
import com.sample.statsapp.models.TransactionStats;
import com.sample.statsapp.models.boundary.TransactionStatsResponse;
import com.sample.statsapp.services.TransactionStatsService;
import org.joda.time.DateTime;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/stats")
@Produces(APPLICATION_JSON)
public class StatsResource {

    private TransactionStatsService transactionStatsService;

    public StatsResource(TransactionStatsService transactionStatsService) {
        this.transactionStatsService = transactionStatsService;
    }

    @GET
    @Timed
    public TransactionStatsResponse getStatistics() {
        TransactionStats statsForLastMinute = transactionStatsService.getStatsForActiveDurationFrom(DateTime.now());

        return TransactionStatsResponse.from(statsForLastMinute);
    }
}
