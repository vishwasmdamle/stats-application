package com.sample.statsapp.resources;

import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/ping")
@Produces(APPLICATION_JSON)
public class PingResource {

    @GET
    @Timed
    public String respondToPing() {
        return "pong";
    }
}
