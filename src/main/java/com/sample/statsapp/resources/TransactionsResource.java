package com.sample.statsapp.resources;

import com.codahale.metrics.annotation.Timed;
import com.sample.statsapp.models.exceptions.StaleTransactionException;
import com.sample.statsapp.models.TransactionRecord;
import com.sample.statsapp.services.TransactionStatsService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;

@Path("/transactions")
@Produces(APPLICATION_JSON)
public class TransactionsResource {

    private TransactionStatsService transactionStatsService;

    public TransactionsResource(TransactionStatsService transactionStatsService) {
        this.transactionStatsService = transactionStatsService;
    }

    @POST
    @Timed
    public Response saveTransaction(@NotNull @Valid TransactionRecord transactionRecord) {
        try {
            transactionStatsService.saveTransaction(transactionRecord);
        } catch (StaleTransactionException e) {
            return Response.status(NO_CONTENT).build();
        }
        return Response.status(CREATED).build();
    }
}
