# Transaction Statistics Application
A plain Dropwizard application to maintain statistics of transactions within last 60 seconds

## What it does:
1. Accepts transaction amount with timestamp on `POST /transactions`.
2. Serves total transaction amount, maximum amount, minimum amount, average & count of all transactions within last 60 seconds on `GET /stats`.

## How it works:
1. It holds transaction stats in buckets/windows of minimum accuracy and updates transaction data as transactions falling in given window arrive.
2. On fetch of stats, it computes accumulated states of all active windows within last 60 seconds and serves
3. This makes it O(1) time and space complexity, involving only space, and time required iterating over "number of windows within 60s"
4. Duration of statistics can be changed. It can also be easily made configurable, if required.
5. It holds this data into memory as problem statement mandates, but it can be refactored to use external tools like Redis cache.

## Assumptions:
1. Application assumes that minimum accuracy is in seconds, which means, it will take transactions of entire 60th second into consideration.
    For example: stats fetched at 60.315s will also include transaction received at 0.210s.
    * But this can be easily tweaked by changing `TransactionStatsService.WINDOW_DURATION_IN_MILLIS` to 10 ms, or even 1 ms, giving maximum possible accuracy at the trade-off of increased (though, constant) time and space of stats computation.
    * The same can be easily made configurable, if required. 
2. Application assumes that the transaction statistics are to be computed from the instance when request is received.  

## How to run:
1. Project makes use of shadowjar gradle plugin to build fat jar.
`gradle clean build shadowJar && java -jar build/libs/stats-api-1.0.0-DEFAULT-all.jar server`. 
3. `test` & `integrationTest` gradle tasks run unit and integration tests respectively.